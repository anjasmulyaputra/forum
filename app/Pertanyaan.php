<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";

    //colom yang boleh di isi
    // protected $fillable = ["judul", "isi"];

    //colom mana saja yang tidak boleh di isi
    protected $guarded = [];

    // public function jawaban() {
    //     return $this->hasOne('App\Jawaban');
    // }

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    
}   




