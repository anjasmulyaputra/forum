<?php

namespace App\Http\Controllers;

use App\Mail\UserVerificationMail;
use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailAjahMail;
use App\Http\Request\Newsletter\SubscriptionFormRequest;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'edit', 'update', 'store', 'destroy']);
    }


    // public function index()
    // {
    //     Mail::to('ihsan@belajarlaravel.test')->send(new EmailAjahMail());
    //     return 'Email Terkirim';
    // }


    
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required',
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        //metode SAVE
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save(); // insert into pertanayan (judul, isi) values

        //METODE MASS ASSIGNMENT
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "user_id" => Auth::user()->id
        ]);




        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil disimpan!');
    }

    public function index () {
    //     // $posts = DB::table('pertanyaan')->get();
    //     // dd($pertanyaan->all());
        
        //pake Model
        $user = Auth::user();

        $pertanyaan = $user->pertanyaan;
        // $pertanyaan = Pertanyaan::all();


        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id) {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        

        //pake Model
        $pertanyaan = Pertanyaan::find($id);

        // dd($pertanyaan->author);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id) {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($post->all())

        $pertanyaan = Pertanyaan::find($id);


        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request) {
        // $request->validate([
        //     'judul' => 'required|unique:pertanyaan|max:255',
        //     'isi' => 'required',
        // ]);
        
        // $query = DB::table('pertanyaan')

        //             ->where('id', $id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'isi' => $request['isi']
        //             ]);
        // dd($post->all())

            //pake MPDEL
        $update = Pertanyaan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);



        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil diupdate!');
    }

    public function destroy($id) {
        
        // $query = DB::table('pertanyaan')

        //             ->where('id', $id)
        //             ->delete();
        // dd($post->all())

        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil dihapus!');
    }

    // public function index()
    // {
    //     return response()->json([
    //         'posts' => [
    //             ['id' => 1, 'tilte' =>'ABC'],
    //             ['id' => 2, 'tilte' =>'DEF'],
    //             ['id' => 1, 'tilte' =>'GHI'],
    //         ]
    //     ]);

    // }

    






}
