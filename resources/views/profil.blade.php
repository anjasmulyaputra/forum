@extends('resume.master2')

@section('content')
<div class="m-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Profil {{$profil->nama_lengkap}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/profil/{{$profil->id}}" method="POST">
                  @csrf
                  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_lengkap"> Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value=" {{ old('nama_lengkap', $profil->nama_lengkap) }} " placeholder="Masukan Nama Lengkap" >
                    @error('nama_lengkap')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value=" {{ old('email', $profil->email) }} " placeholder="Masukan Email">
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="foto">Foto Profil</label>
                    <input type="file" class="form-control" id="foto" name="foto" value=" {{ old('foto', $profil->foto) }} " placeholder="Unggah Foto">
                    @error('foto')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            </div>

@endsection