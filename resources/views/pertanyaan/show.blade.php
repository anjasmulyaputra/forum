@extends('resume.master2')

@section('content')

<div class="resume-section-content m-5">
                    <h2 class="mb-0">
                        Questions <br>
                        <span class="text-primary">{{ $pertanyaan -> judul }}</span>
                    </h2>
                    <div class="subheading mb-5">
                    <p> {{ $pertanyaan -> isi }} </p>
                    <p> Author : {{ $pertanyaan->author->name }}</p>
                    </div>

                     <!-- /.card-footer -->
              <div class="card-footer">
                <form action="#" method="post">
                  <img class="img-fluid img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push">
                    <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
                  </div>
                </form>
              </div>
              <!-- /.card-footer -->

                    
                </div>

               
    
@endsection