@extends('resume.master2')

@section('content')
<div class="m-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create a New Question</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
                  @csrf
                  
                <div class="card-body">
                <a href="/pertanyaan/" class="btn btn-primary mb-2">List a Question</a>
                  <div class="form-group">
                    <label for="judul"> Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value=" {{ old('judul', '') }} " placeholder="Masukan Judul" >
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" value=" {{ old('isi', '') }} " placeholder="Masukan pertanyaan">
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
            </div>

@endsection