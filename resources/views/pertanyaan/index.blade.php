@extends('resume.master2')

@section('content')
    <div class="m-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Table Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                

                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <a href="{{ route('pertanyaan.create') }}" class="btn btn-primary mb-2">Create a Question</a>
                <!-- <a href="/pertanyaan/create" class="btn btn-info mt-3 mb-2">Create a New Question</a> -->
                <table class="table table-bordered">
                  <thead >                  
                    <tr style="text-align: center;">
                      <th style="width: 10px">No.</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th colspan="3" class="justify-content-center" >Pilihan</th>
                    </tr>
                  </thead>
                  <tbody>
                  @forelse($pertanyaan as $key => $pertanyaan)
                  <tr>
                      <td> {{ $key + 1 }}</td>
                      <td> {{ $pertanyaan -> judul }} </td>
                      <td> {{ $pertanyaan -> isi }} </td>
                      <td><a href=" {{ route('pertanyaan.show', ['pertanyaan' => $pertanyaan->id]) }}"><i class=" fa fa-eye "></i></a> </td>
                      <td><a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-dark btn-sm">Edit</a></td> 
                      <td><form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form></td>
                      
                  </tr>
                  @empty
                  <tr>
                  <td colspan="4" align="center">Tidak ada pertanyaan</td>
                  </tr>
                    
                  @endforelse
                   
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>




    </div>
@endsection